package com.moravskiyandriy;

public class Constants {
    private Constants() {
    }

    public static final String ENDPOINT="http://localhost:5554/Andriy_MoravskiyServer/accounts/";
    public static final String ACCOUNT_CREATED = "was successfully added.";
    public static final String ACCOUNT_NOT_CREATED = "Account can not be created.";
    public static final String ACCOUNT_NOT_FOUND = "Account was not found.";
    public static final String ACCOUNT_DELETED = "Account was deleted successfully.";
    public static final String NO_ACCOUNT_WITH_SUCH_ID = "No account with such id exists.";
    public static final String SUM_TOO_SMALL = "Sum is too small";
    public static final String BALANCE_REPLENISHED_OK = "Balance was successfully replenished.";
    public static final String BALANCE_REPLENISHED_BAD = "Balance can not be replenished due to limitations.";
    public static final String BALANCE_EXHAUSTED_OK = "Balance was successfully exhausted.";
    public static final String BALANCE_EXHAUSTED_BAD = "Balance can not be exhausted due to limitations.";
}
