package com.moravskiyandriy.client;

import com.moravskiyandriy.BaseClient;
import com.moravskiyandriy.Constants;
import org.apache.cxf.jaxrs.client.WebClient;

import javax.ws.rs.core.Response;
import java.util.Optional;

class RESTClient extends BaseClient {
    private static String endpointUrl = Optional.
            ofNullable(getProperties().getProperty("ENDPOINT"))
            .orElse(Constants.ENDPOINT);
    private static WebClient client;

    Response getResponse(String uri) {
        client = WebClient.create(endpointUrl + uri);
        return client.accept("text/plain").get();
    }
}
