package com.moravskiyandriy.client;

import com.moravskiyandriy.BaseClient;
import com.moravskiyandriy.Constants;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.util.Optional;

class SOAPClient extends BaseClient {
    private final String soapEndpointUrl = Optional.
            ofNullable(getProperties().getProperty("ENDPOINT"))
            .orElse(Constants.ENDPOINT)+"?wsdl";
    private final String soapAction = "\"\"";

    String getResponse(String action, String[] arguments) {
        return callSoapWebService(soapEndpointUrl, soapAction, action, arguments);
    }

    private static void createSoapEnvelope(SOAPMessage soapMessage, String action, String[] arguments) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "hs";
        String myNamespaceURI = "http://service.moravskiyandriy.com/";

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(action, myNamespace);
        int index = 0;
        for (String s : arguments) {
            SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("arg" + index);
            soapBodyElem1.addTextNode(s);
            index++;
        }
    }

    private static String callSoapWebService(String soapEndpointUrl, String soapAction, String action, String[] arguments) {
        String message;
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, action, arguments), soapEndpointUrl);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            message = new String(out.toByteArray());

            soapConnection.close();
        } catch (Exception e) {
            message = "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n";
        }
        return message;
    }

    private static SOAPMessage createSOAPRequest(String soapAction, String action, String[] arguments) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, action, arguments);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        return soapMessage;
    }
}