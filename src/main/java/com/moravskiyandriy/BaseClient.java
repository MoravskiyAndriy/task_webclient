package com.moravskiyandriy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class BaseClient {
    private static final Logger logger = LogManager.getLogger(BaseClient.class);

    protected static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = BaseClient.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("NumberFormatException found.");
            }
        } catch (IOException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("IOException found.");
            }
        }
        return prop;
    }
}
