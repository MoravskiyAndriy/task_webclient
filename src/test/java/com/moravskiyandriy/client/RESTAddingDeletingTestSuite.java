package com.moravskiyandriy.client;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory({RESTClientTest.AddingTests.class,
        RESTClientTest.DeletingTests.class})
@Suite.SuiteClasses({RESTClientTest.class})
public class RESTAddingDeletingTestSuite {
}



