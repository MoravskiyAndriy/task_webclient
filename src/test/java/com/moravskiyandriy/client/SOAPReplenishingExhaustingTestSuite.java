package com.moravskiyandriy.client;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory({SOAPClientTests.ReplenishingTests.class,
        SOAPClientTests.ExhaustingTests.class})
@Suite.SuiteClasses({SOAPClientTests.class})
public class SOAPReplenishingExhaustingTestSuite {
}
