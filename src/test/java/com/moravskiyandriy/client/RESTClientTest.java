package com.moravskiyandriy.client;

import com.moravskiyandriy.Constants;
import org.apache.cxf.helpers.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.DisplayName;


import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


public class RESTClientTest {
    interface ReplenishingTests {
    }

    interface ExhaustingTests {
    }

    interface AddingTests {
    }

    interface DeletingTests {
    }

    private static RESTClient client = new RESTClient();

    @Before
    public void Init() throws IOException {
        Response r = client.getResponse("/getAllAccounts");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String accountsJSON = IOUtils.toString((InputStream) r.getEntity());
        if(accountsJSON.contains("[")) {
            JSONArray jSONArray = new JSONArray(accountsJSON);
            List<Integer> list = new ArrayList<>();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject object = (JSONObject) jSONArray.get(i);
                int val = object.getInt("id");
                list.add(val);
            }
            for (Integer i : list) {
                r = client.getResponse("/deleteAccount/" + i);
            }
            r = client.getResponse("/addAccount/100");
            r = client.getResponse("/addAccount/200");
        }
    }

    @DisplayName("replenish Account: OK")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestOK() throws IOException {
        Response r = client.getResponse("/replenishAccount/1/50");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_REPLENISHED_OK,value);
    }

    @DisplayName("replenish Account: OK [limit]")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestOKLimit() throws IOException {
        Response r = client.getResponse("/replenishAccount/1/900");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_REPLENISHED_OK, value);
    }

    @DisplayName("replenish Account: Small sum")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestSmallSum() throws IOException {
        Response r = client.getResponse("/replenishAccount/1/0.01");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.SUM_TOO_SMALL, value);
    }

    @DisplayName("replenish Account: Bad")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestBad() throws IOException {
        Response r = client.getResponse("/replenishAccount/1/10000");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_REPLENISHED_BAD, value);
    }

    @DisplayName("replenish Account: Bad [limit]")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestBadLimit() throws IOException {
        Response r = client.getResponse("/replenishAccount/1/900.01");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_REPLENISHED_BAD, value);
    }

    @DisplayName("exhaust Account: OK")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestOK() throws IOException {
        Response r = client.getResponse("/exhaustAccount/1/50");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_EXHAUSTED_OK, value);
    }

    @DisplayName("exhaust Account: OK [limit]")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestOKLimit() throws IOException {
        Response r = client.getResponse("/exhaustAccount/1/150");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_EXHAUSTED_OK, value);
    }

    @DisplayName("exhaust Account: Small sum")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestSmallSum() throws IOException {
        Response r = client.getResponse("/exhaustAccount/1/0.01");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.SUM_TOO_SMALL, value);
    }

    @DisplayName("exhaust Account: Bad")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestBad() throws IOException {
        Response r = client.getResponse("/exhaustAccount/1/1000");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_EXHAUSTED_BAD, value);
    }

    @DisplayName("exhaust Account: Bad [limit]")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestBadLimit() throws IOException {
        Response r = client.getResponse("/exhaustAccount/1/150.01");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.BALANCE_EXHAUSTED_BAD, value);
    }

    @DisplayName("add Account: OK")
    @Category({AddingTests.class})
    @Test
    public void addAccountTestOK() throws IOException {
        Response r = client.getResponse("/addAccount/10");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertTrue(value.contains(Constants.ACCOUNT_CREATED));
    }

    @DisplayName("add Account: Bad: Too Big Sum")
    @Category({AddingTests.class})
    @Test
    public void addAccountTestBadTooBigSum() throws IOException {
        Response r = client.getResponse("/addAccount/10000");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.ACCOUNT_NOT_CREATED, value);
    }

    @DisplayName("add Account: Bad: Too Small Sum")
    @Category({AddingTests.class})
    @Test
    public void addAccountTestBadTooSmallSum() throws IOException {
        Response r = client.getResponse("/addAccount/-50");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.SUM_TOO_SMALL, value);
    }

    @DisplayName("delete Account: OK")
    @Category({DeletingTests.class})
    @Test
    public void deleteAccountTestOK() throws IOException {
        Response r = client.getResponse("/deleteAccount/2");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.ACCOUNT_DELETED, value);
    }

    @DisplayName("delete Account: Bad")
    @Category({DeletingTests.class})
    @Test
    public void deleteAccountTestBad() throws IOException {
        Response r = client.getResponse("/deleteAccount/3");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.ACCOUNT_NOT_FOUND, value);
    }

    @DisplayName("get Account balance: Bad")
    @Test
    public void getAccountBalanceTestBad() throws IOException {
        Response r = client.getResponse("/getBalance/100");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        assertEquals(Constants.NO_ACCOUNT_WITH_SUCH_ID, value);
    }

    @AfterClass
    public static void afterClass() throws IOException {
        Response r = client.getResponse("/getAllAccounts");
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        String value = IOUtils.toString((InputStream) r.getEntity());
        if(value.contains("[")) {
            JSONArray jSONArray = new JSONArray(value);
            List<Integer> list = new ArrayList<>();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject object = (JSONObject) jSONArray.get(i);
                int val = object.getInt("id");
                list.add(val);
            }
            for (Integer i : list) {
                r = client.getResponse("/deleteAccount/" + i);
            }
        }
    }
}
