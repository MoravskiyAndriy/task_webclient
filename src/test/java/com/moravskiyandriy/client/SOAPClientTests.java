package com.moravskiyandriy.client;

import com.moravskiyandriy.Constants;
import com.moravskiyandriy.xmlreader.XMLReader;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.DisplayName;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class SOAPClientTests {
    private static SOAPClient client = new SOAPClient();

    interface ReplenishingTests {
    }

    interface ExhaustingTests {
    }

    interface AddingTests {
    }

    interface DeletingTests {
    }

    @Before
    public void Init() throws IOException, SAXException, ParserConfigurationException {
        client.getResponse("addAccount", new String[]{"100"});
        String response = client.getResponse("getAllAccounts", new String[]{});
        List<String> output = XMLReader.getElementFromXml(response, "id");
        List<Integer> IdList = output.stream().map(Integer::valueOf).collect(Collectors.toList());
        for(Integer i: IdList){
            client.getResponse("deleteAccountById",new String[]{i.toString()});
        }
        client.getResponse("addAccount", new String[]{"100"});
        client.getResponse("addAccount", new String[]{"200"});
    }

    @DisplayName("replenish Account: OK")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestOK() {
        String response = client.getResponse("replenish", new String[]{"1", "50.0"});
        assertTrue(response.contains(Constants.BALANCE_REPLENISHED_OK));
    }

    @DisplayName("replenish Account: OK [limit]")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestOKLimit() {
        String response = client.getResponse("replenish", new String[]{"1", "900"});
        assertTrue(response.contains(Constants.BALANCE_REPLENISHED_OK));
    }

    @DisplayName("replenish Account: Small sum")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestSmallSum() {
        String response = client.getResponse("replenish", new String[]{"1", "0.01"});
        assertTrue(response.contains(Constants.SUM_TOO_SMALL));
    }

    @DisplayName("replenish Account: Bad")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestBad() {
        String response = client.getResponse("replenish", new String[]{"1", "1000"});
        assertTrue(response.contains(Constants.BALANCE_REPLENISHED_BAD));
    }

    @DisplayName("replenish Account: Bad [limit]")
    @Category({ReplenishingTests.class})
    @Test
    public void replenishTestBadLimit() {
        String response = client.getResponse("replenish", new String[]{"1", "900.01"});
        assertTrue(response.contains(Constants.BALANCE_REPLENISHED_BAD));
    }

    @DisplayName("exhaust Account: OK")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestOK() {
        String response = client.getResponse("exhaust", new String[]{"1", "50.00"});
        assertTrue(response.contains(Constants.BALANCE_EXHAUSTED_OK));
    }

    @DisplayName("exhaust Account: OK [limit]")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestOKLimit() {
        String response = client.getResponse("exhaust", new String[]{"1", "150.00"});
        assertTrue(response.contains(Constants.BALANCE_EXHAUSTED_OK));
    }

    @DisplayName("exhaust Account: Small sum")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestSmallSum() {
        String response = client.getResponse("exhaust", new String[]{"1", "0.01"});
        assertTrue(response.contains(Constants.SUM_TOO_SMALL));
    }

    @DisplayName("exhaust Account: Bad")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestBad() {
        String response = client.getResponse("exhaust", new String[]{"1", "1000.00"});
        assertTrue(response.contains(Constants.BALANCE_EXHAUSTED_BAD));
    }

    @DisplayName("exhaust Account: Bad [limit]")
    @Category({ExhaustingTests.class})
    @Test
    public void exhaustTestBadLimit() {
        String response = client.getResponse("exhaust", new String[]{"1", "150.01"});
        assertTrue(response.contains(Constants.BALANCE_EXHAUSTED_BAD));
    }

    @DisplayName("add Account: OK")
    @Category({AddingTests.class})
    @Test
    public void addAccountTestOK() {
        String response = client.getResponse("addAccount", new String[]{"10"});
        assertTrue(response.contains(Constants.ACCOUNT_CREATED));
    }

    @DisplayName("add Account: Bad: Too Big Sum")
    @Category({AddingTests.class})
    @Test
    public void addAccountTestBadTooBigSum() {
        String response = client.getResponse("addAccount", new String[]{"10000"});
        assertTrue(response.contains(Constants.ACCOUNT_NOT_CREATED));
    }

    @DisplayName("add Account: Bad: Too Small Sum")
    @Category({AddingTests.class})
    @Test
    public void addAccountTestBadTooSmallSum() {
        String response = client.getResponse("addAccount", new String[]{"-50"});
        assertTrue(response.contains(Constants.SUM_TOO_SMALL));
    }

    @DisplayName("delete Account: OK")
    @Category({DeletingTests.class})
    @Test
    public void deleteAccountTestOK() {
        String response = client.getResponse("deleteAccountById", new String[]{"2"});
        assertTrue(response.contains(Constants.ACCOUNT_DELETED));
    }

    @DisplayName("delete Account: Bad")
    @Category({DeletingTests.class})
    @Test
    public void deleteAccountTestBad() {
        String response = client.getResponse("deleteAccountById", new String[]{"3"});
        assertTrue(response.contains(Constants.ACCOUNT_NOT_FOUND));
    }

    @DisplayName("get Account balance: Bad")
    @Test
    public void getAccountBalanceTestBad() {
        String response = client.getResponse("getBalance", new String[]{"3"});
        assertTrue(response.contains(Constants.NO_ACCOUNT_WITH_SUCH_ID));
    }

    @AfterClass
    public static void afterClass() throws IOException, SAXException, ParserConfigurationException {
        client.getResponse("addAccount", new String[]{"100"});
        String response = client.getResponse("getAllAccounts", new String[]{});
        List<String> output = XMLReader.getElementFromXml(response, "id");
        List<Integer> IdList = output.stream().map(Integer::valueOf).collect(Collectors.toList());
        for(Integer i: IdList){
            client.getResponse("deleteAccountById",new String[]{i.toString()});
        }
    }
}
